#ifndef TELECOMMAND_H_
#define TELECOMMAND_H_

#include <mbed.h>
#include "config.h"
#include "constants.h"
#include "frame.h"
#include "network.h"

class Network;
class Frame;

class TelecommandProducer
{
public:
    TelecommandProducer(Network *network);
    void send(unsigned int target_node_id, unsigned char* data, unsigned char length);

protected:
    Network *network;
};

class TelecommandConsumer
{
public:
    TelecommandConsumer(Network *network);
    bool read(Frame &f);
    bool any();

protected:
    Network *network;
    CircularBuffer<Frame, TC_BUFFER_SIZE> buffer;
    void received(Frame frame);
};

#endif /* TELECOMMAND_H_ */
