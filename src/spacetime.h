#ifndef SPACETIME_H_
#define SPACETIME_H_

#include "config.h"
#include "mbed.h"
#include "frame.h"
#include "constants.h"
#include "network.h"

class Time
{
public:
    Time(Network *network);

    virtual void update_from_frame(Frame frame) = 0;
    virtual void send() = 0;

protected:
    Network *network;
};

class Utc : public Time
{
public:
    Utc(Network *network, unsigned int day=0, unsigned int ms_of_day=0, float sub_of_ms=0);

    void send();
    void update_from_frame(Frame frame);
    unsigned int day;
    unsigned int ms_of_day;
    float sub_of_ms;
};

class Scet : public Time
{
public:
    Scet(Network *network, unsigned int seconds=0, float subseconds=0, unsigned int precision=PRECISION_24_BITS);

    void send();
    void update_from_frame(Frame frame);
    unsigned int seconds;
    float subseconds;
    unsigned int precision;
};

#endif /* SPACETIME_H_ */
