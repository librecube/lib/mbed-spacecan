#include "heartbeat.h"

Heartbeat::Heartbeat(Network *network)
:   network(network), running(false), led(DigitalOut(LED1))
{}

void Heartbeat::start(int period)
{
    if (this->running)
    {
        this->stop();
    }
    this->timer.attach(callback(this, &Heartbeat::timer_callback), period/1000.0f);
    this->running = true;
}

void Heartbeat::stop()
{
    this->timer.detach();
    this->running = false;
}

HeartbeatProducer::HeartbeatProducer(Network *network)
:   Heartbeat(network)
{
    unsigned char data = 0;
    this->frame = Frame(ID_HEARTBEAT + this->network->node_id, &data, 1);
}

void HeartbeatProducer::timer_callback()
{
    this->send();
}

void HeartbeatProducer::send()
{
    if (this->network->is_active() && LED_HEARTBEAT)
    {
        this->led = !this->led;
    }
    this->network->send(this->frame);
}

HeartbeatConsumer::HeartbeatConsumer(Network *network, int max_heartbeat_missed, int max_bus_switches)
:   Heartbeat(network),
    max_heartbeat_missed(max_heartbeat_missed),
    max_bus_switches(max_bus_switches),
    heartbeat_missed(0),
    bus_switches(0),
    received_frame(false)
{
    this->network->register_heartbeat_handler(callback(this, &HeartbeatConsumer::received));
}

void HeartbeatConsumer::timer_callback()
{
    if(!this->received_frame)
    {
        // TODO: look into possible race condition
        this->received_frame = false;
        this->heartbeat_missed++;

        if (this->heartbeat_missed >= this->max_heartbeat_missed)
        {
            // check if bus switch is allowed
            if ((this->bus_switches < this->max_bus_switches) || !this->max_bus_switches)
            {
                this->network->switch_bus();
                this->bus_switches++;
                this->heartbeat_missed = 0;
            }
        }
    }
    else
    {
        // weird to do the same thing in if and else...
        this->received_frame = false;
    }
}

void HeartbeatConsumer::received(Frame frame)
{
    if (this->running)
    {
        if (LED_HEARTBEAT)
        {
            this->led = !this->led;
        }
        this->received_frame = true;
        this->heartbeat_missed = 0;
        this->bus_switches = 0;
    }
}
