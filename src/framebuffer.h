#ifndef FRAMEBUFFER_H_
#define FRAMEBUFFER_H_

#include <mbed.h>
#include "config.h"
#include "bus.h"
#include "frame.h"

class Bus;

class FrameBuffer
{
public:
    FrameBuffer();
    void buffer_frame(Bus *bus);
    bool read(Frame &f);
    bool any();
    void clear();

private:
    CircularBuffer<Frame, FRAME_BUFFER_SIZE> buffer;
    CANMessage msg_buf;
};

#endif /* FRAMEBUFFER_H_*/
