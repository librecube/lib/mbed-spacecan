#ifndef HEARTBEAT_H_
#define HEARTBEAT_H_

#include <mbed.h>
#include "config.h"
#include "constants.h"
#include "frame.h"
#include "network.h"

class Network;
class Frame;

class Heartbeat
{
public:
    Heartbeat(Network *network);
    void start(int period=HEARTBEAT_PERIOD);
    void stop();

protected:
    Network *network;
    Ticker timer;
    bool running;
    DigitalOut led;
    virtual void timer_callback() = 0;
};

class HeartbeatProducer : public Heartbeat
{
public:
    HeartbeatProducer(Network *network);

protected:
    Frame frame;
    void timer_callback();
    void send();
};

class HeartbeatConsumer : public Heartbeat
{
public:
    HeartbeatConsumer(
      Network *network,
      int max_heartbeat_missed=MAX_HEARTBEAT_MISSED,
      int max_bus_switches=MAX_BUS_SWITCHES
    );
    void stop();

protected:
    int max_heartbeat_missed;
    int max_bus_switches;
    int heartbeat_missed;
    int bus_switches;
    bool received_frame;
    void timer_callback();
    void received(Frame frame);
};

#endif /* HEARTBEAT_H_*/
