#ifndef SYNC_H_
#define SYNC_H_

#include <mbed.h>
#include "config.h"
#include "constants.h"
#include "frame.h"
#include "network.h"

class Network;
class Frame;

class Sync
{
public:
    Sync(Network *network);

protected:
    Network *network;
    bool running;
    DigitalOut led;
};

class SyncProducer : public Sync
{
public:
    SyncProducer(Network *network);
    void start(int period);
    void stop();
    void send();
    bool sent();

protected:
    Ticker timer;
    Frame frame;
    bool sync_sent;
    void timer_callback();
};

class SyncConsumer : public Sync
{
public:
    SyncConsumer(Network *network);
    void start();
    void stop();
    bool is_received();

protected:
    bool sync_received;
    void received(Frame frame);
};

#endif /* SYNC_H_ */
