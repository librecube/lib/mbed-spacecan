#ifndef NETWORK_H_
#define NETWORK_H_

#include <mbed.h>
#include "config.h"
#include "constants.h"
#include "framebuffer.h"
#include "bus.h"

class Frame;
class FrameBuffer;
class Bus;
typedef Callback<void(Frame)> frame_handler_t;

class Network
{
public:
    Network(unsigned int node_id, Bus *bus_a, Bus *bus_b);
    unsigned int node_id;
    FrameBuffer *frame_buffer;
    void set_filters(unsigned int *filters, int len_filters);
    Bus* get_active_bus();
    void set_active_bus(Bus &bus);
    void switch_bus();
    void start();
    void stop();
    bool is_active();
    void shutdown();
    void send(Frame frame);
    void process();
    void register_heartbeat_handler(frame_handler_t handler);
    void register_sync_handler(frame_handler_t handler);
    void register_scet_handler(frame_handler_t handler);
    void register_utc_handler(frame_handler_t handler);
    void register_tc_handler(frame_handler_t handler);
    void register_tm_handler(frame_handler_t handler);

protected:
    Bus *bus_a;
    Bus *bus_b;
    Bus *active_bus;
    bool active;
    frame_handler_t heartbeat_handler;
    frame_handler_t sync_handler;
    frame_handler_t scet_handler;
    frame_handler_t utc_handler;
    frame_handler_t tc_handler;
    frame_handler_t tm_handler;
};

#endif /* NETWORK_H_ */
