#include "framebuffer.h"

FrameBuffer::FrameBuffer() : msg_buf(CANMessage()) {}

void FrameBuffer::buffer_frame(Bus *bus)
{
    if (this->buffer.full())
    {
        bus->can->read(this->msg_buf);  // discard frame
    }
    else
    {
        if (bus->can->read(this->msg_buf) == 1)
        {
            Frame f = Frame(this->msg_buf.id, this->msg_buf.data, this->msg_buf.len);
            this->buffer.push(f);
        }
    }
}

bool FrameBuffer::read(Frame &f)
{
    return this->buffer.pop(f);
}

bool FrameBuffer::any()
{
    return !(this->buffer.empty());
}

void FrameBuffer::clear()
{
    this->buffer.reset();
}
