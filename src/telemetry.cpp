#include "telemetry.h"

TelemetryProducer::TelemetryProducer(Network *network) : network(network)
{}

void TelemetryProducer::send(unsigned char* data, unsigned char length)
{
    if (this->network->is_active())
    {
        Frame frame(ID_TM + this->network->node_id, data, length);
        this->network->send(frame);
    }
}

TelemetryConsumer::TelemetryConsumer(Network *network) : network(network)
{
    this->network->register_tm_handler(callback(this, &TelemetryConsumer::received));
}

bool TelemetryConsumer::read(Frame &frame)
{
    return this->buffer.pop(frame);
}

bool TelemetryConsumer::any()
{
    return !(this->buffer.empty());
}

void TelemetryConsumer::received(Frame frame)
{
    this->buffer.push(frame);
}
