#ifndef BUS_H_
#define BUS_H_


#include <mbed.h>
#include "constants.h"
#include "frame.h"
#include "network.h"



class Network;
class Frame;

class Bus
{
public:
    Bus(PinName rd, PinName td, int bitrate=1000);
    CAN *can;
    Network *network;
    void send(Frame frame);
    void set_filters(unsigned int *filters, int len_filters);
    void start();
    void stop();
    void restart();
    void shutdown();

protected:
    void clear_filters();
    void rxcallback();
};

#endif /* BUS_H_ */
