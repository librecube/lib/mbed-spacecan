#include "telecommand.h"

TelecommandProducer::TelecommandProducer(Network *network) : network(network)
{}

void TelecommandProducer::send(unsigned int target_node_id, unsigned char* data, unsigned char length)
{
    if (this->network->is_active())
    {
        Frame frame(ID_TC + target_node_id, data, length);
        this->network->send(frame);
    }
}

TelecommandConsumer::TelecommandConsumer(Network *network) : network(network)
{
    this->network->register_tc_handler(callback(this, &TelecommandConsumer::received));
}

bool TelecommandConsumer::read(Frame &frame)
{
    return this->buffer.pop(frame);
}

bool TelecommandConsumer::any()
{
    return !(this->buffer.empty());
}

void TelecommandConsumer::received(Frame frame)
{
    this->buffer.push(frame);
}
