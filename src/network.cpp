#include "network.h"

FrameBuffer frame_buff;

Network::Network(unsigned int node_id, Bus *bus_a, Bus *bus_b)
:   node_id(node_id), bus_a(bus_a), bus_b(bus_b), active_bus(bus_a), active(false),
    heartbeat_handler(NULL),
    sync_handler(NULL),
    scet_handler(NULL),
    utc_handler(NULL),
    tc_handler(NULL),
    tm_handler(NULL)
{
    this->frame_buffer = &frame_buff;
    this->bus_a->network = this;
    if(this->bus_b)
    {
        this->bus_b->network = this;
    }
}

void Network::set_filters(unsigned int *filters, int len_filters)
{
    this->bus_a->set_filters(filters, len_filters);
    if(this->bus_b)
    {
        this->bus_b->set_filters(filters, len_filters);
    }
}

Bus* Network::get_active_bus()
{
    return this->active_bus;
}

void Network::set_active_bus(Bus &bus)
{

}

void Network::switch_bus()
{
    this->stop();
    if(this->bus_b && this->active_bus == this->bus_a)
    {
        this->active_bus = this->bus_b;

    }
    else
    {
        this->active_bus = this->bus_a;
    }
    this->active_bus->restart();
    this->start();
}

void Network::start()
{
    this->active = true;
    this->active_bus->start();
}

void Network::stop()
{
    this->active = false;
    this->active_bus->stop();
}

bool Network::is_active()
{
    return this->active;
}

void Network::shutdown()
{
    this->active = false;
    this->bus_a->restart();
    if(this->bus_b)
    {
        this->bus_b->restart();
    }
}

void Network::send(Frame frame)
{
    if (this->active)
    {
        this->active_bus->send(frame);
    }
}

void Network::process()
{
    Frame frame;
    if (!(this->frame_buffer->read(frame)))
    {
        return;
    }
    unsigned int can_id = frame.can_id;
    unsigned int cob_id = can_id & FUNCTION_MASK;

    if (cob_id == ID_HEARTBEAT)
    {
        if (this->heartbeat_handler)
        {
            this->heartbeat_handler(frame);
        }
    }
    else if (cob_id == ID_SYNC)
    {
        if (this->sync_handler)
        {
            this->sync_handler(frame);
        }
    }
    else if (cob_id == ID_SCET)
    {
        if (this->scet_handler)
        {
            this->scet_handler(frame);
        }
    }
    else if (cob_id == ID_UTC)
    {
        if (this->utc_handler)
        {
            this->utc_handler(frame);
        }
    }
    else if (cob_id == ID_TC)
    {
        if (this->tc_handler)
        {
            this->tc_handler(frame);
        }
    }
    else if (cob_id == ID_TM)
    {
        if (this->tm_handler)
        {
            this->tm_handler(frame);
        }
    }
}

void Network::register_heartbeat_handler(frame_handler_t handler)
{
    this->heartbeat_handler = handler;
}

void Network::register_sync_handler(frame_handler_t handler)
{
    this->sync_handler = handler;
}

void Network::register_scet_handler(frame_handler_t handler)
{
    this->scet_handler = handler;
}

void Network::register_utc_handler(frame_handler_t handler)
{
    this->utc_handler = handler;
}

void Network::register_tc_handler(frame_handler_t handler)
{
    this->tc_handler = handler;
}

void Network::register_tm_handler(frame_handler_t handler)
{
    this->tm_handler = handler;
}
