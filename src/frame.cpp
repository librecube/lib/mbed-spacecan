#include "frame.h"

Frame::Frame(unsigned int can_id, unsigned char* data, unsigned int length)
:   can_id(can_id)
{
    if (length <= 8)
    {
        this->length = length;
    }
    else
    {
        // TODO: maybe throw exception
        this->length = 8;
    }
    for (unsigned int i = 0; i < length; i++)
    {
        this->data[i] = data[i];
    }
}

Frame::Frame() : can_id(0), length(0) {}
