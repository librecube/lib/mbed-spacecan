#ifndef TELEMETRY_H_
#define TELEMETRY_H_

#include <mbed.h>
#include "config.h"
#include "constants.h"
#include "frame.h"
#include "network.h"

class Network;
class Frame;

class TelemetryProducer
{
public:
    TelemetryProducer(Network *network);
    void send(unsigned char* data, unsigned char length);

protected:
    Network *network;
};

class TelemetryConsumer
{
public:
    TelemetryConsumer(Network *network);
    bool read(Frame &f);
    bool any();

protected:
    Network *network;
    CircularBuffer<Frame, TM_BUFFER_SIZE> buffer;
    void received(Frame frame);
};

#endif /* TELEMETRY_H_ */
