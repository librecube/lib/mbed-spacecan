#include "spacetime.h"

Time::Time(Network *network)
    : network(network)
{}

Utc::Utc(Network *network, unsigned int day, unsigned int ms_of_day, float sub_of_ms)
    : Time(network), day(day), ms_of_day(ms_of_day), sub_of_ms(sub_of_ms)
{}

void Utc::update_from_frame(Frame frame)
{
    float sub_of_ms = (frame.data[0] << 8) + frame.data[1];

    this->ms_of_day =
        (frame.data[2] << 24) +
        (frame.data[3] << 16) +
        (frame.data[4] << 8) +
        frame.data[5];

    this->day = (frame.data[6] << 8) + frame.data[7];

    this->sub_of_ms = sub_of_ms / PRECISION_16_BITS;
}

void Utc::send()
{
    unsigned char data[8];
    unsigned int sub_of_ms = this->sub_of_ms * PRECISION_16_BITS;

    data[0] = sub_of_ms >> 8;
    data[1] = sub_of_ms & 0xFF;

    data[2] = this->ms_of_day >> 24;
    data[3] = (this->ms_of_day >> 16) & 0xFF;
    data[4] = (this->ms_of_day >> 8) & 0xFF;
    data[5] = this->ms_of_day & 0xFF;

    data[6] = this->day >> 8;
    data[7] = this->day & 0xFF;

    this->network->send(Frame((ID_UTC + this->network->node_id), data, 8));
}

Scet::Scet(Network *network, unsigned int seconds, float subseconds, unsigned int precision)
    : Time(network), seconds(seconds), subseconds(subseconds), precision(precision)
{}

void Scet::update_from_frame(Frame frame)
{
    unsigned int fine_time = 0;
    if(this->precision == PRECISION_8_BITS)
    {
        fine_time = frame.data[0];
    }
    else if(this->precision == PRECISION_16_BITS)
    {
        fine_time =
            (frame.data[0] << 8) +
            frame.data[1];
    }
    else if(this->precision == PRECISION_24_BITS)
    {
        fine_time =
            (frame.data[0] << 16) +
            (frame.data[1] << 8) +
            frame.data[2];
    }

    this->seconds =
        (frame.data[3] << 24) +
        (frame.data[4] << 16) +
        (frame.data[5] << 8) +
        frame.data[6];
    this->subseconds = ((float)fine_time)/this->precision;
}

void Scet::send()
{
    unsigned int fine_time = this->subseconds * this->precision;
    
    if(this->precision == PRECISION_8_BITS)
    {
        fine_time = fine_time << 16;
    }
    else if(this->precision == PRECISION_16_BITS)
    {
        fine_time = fine_time << 8;
    }
    unsigned char data[7];

    data[0] = fine_time >> 16;
    data[1] = (fine_time >> 8) & 0xFF;
    data[2] = fine_time & 0xFF;

    data[3] = this->seconds >> 24;
    data[4] = (this->seconds >> 16) & 0xFF;
    data[5] = (this->seconds >> 8) & 0xFF;
    data[6] = this->seconds & 0xFF;

    this->network->send(Frame((ID_SCET + this->network->node_id), data, 7));
}
