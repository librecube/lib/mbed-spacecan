#include "bus.h"

#define TOTAL_FILTERBANKS 14

Bus::Bus(PinName rd, PinName td, int bitrate)
:   network(NULL)
{
    this->can = new CAN(rd, td, bitrate * 1000);

    this->start();
}

void Bus::send(Frame frame)
{
    this->can->write(CANMessage(frame.can_id, frame.data, frame.length));
}

void Bus::set_filters(unsigned int *filters, int len_filters)
{
    if (len_filters <= TOTAL_FILTERBANKS)
    {
        for(int i = 0; i < len_filters; i++)
        {
            this->can->filter(filters[i], FULL_MASK, CANStandard, i);
        }
    }
}

void Bus::clear_filters()
{
    unsigned int filters[TOTAL_FILTERBANKS] = {0};
    this->set_filters(filters, TOTAL_FILTERBANKS);
}

void Bus::rxcallback()
{
    if (this->network && this->network->is_active())
    {
        this->network->frame_buffer->buffer_frame(this);
    }
    else
    {
        CANMessage msg;
        this->can->read(msg);  // read but ignore the frame
    }
}

void Bus::start()
{
    this->can->attach(callback(this, &Bus::rxcallback), CAN::RxIrq);
}

void Bus::stop()
{
    this->can->attach(NULL, CAN::RxIrq);
}

void Bus::restart()
{
    this->can->reset(); //TODO: check what reset does and if it needed
}
