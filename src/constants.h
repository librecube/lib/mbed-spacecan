#ifndef CONSTANTS_H_
#define CONSTANTS_H_

#define FULL_MASK       0x7FF
#define FUNCTION_MASK   0x780
#define NODE_MASK       0x07F

#define ID_SYNC         0x080
#define ID_HEARTBEAT    0x700
#define ID_SCET         0x180
#define ID_UTC          0x200
#define ID_TC           0x280
#define ID_TM           0x300
#define ID_MESSAGE      0x380

#define PRECISION_8_BITS    0x100
#define PRECISION_16_BITS   0x10000
#define PRECISION_24_BITS   0x1000000

#endif /* CONSTANTS_H_ */
