#ifndef FRAME_H_
#define FRAME_H_

class Frame
{
public:
    Frame(unsigned int can_id, unsigned char* data, unsigned int length);
    Frame(); // CircularBuffer template needs default constructor
    unsigned int can_id;
    char data[8];
    unsigned int length;
};

#endif /* FRAME_H_*/
