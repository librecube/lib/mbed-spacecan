#include "sync.h"

Sync::Sync(Network *network)
:   network(network), running(false), led(DigitalOut(LED1))
{}

SyncProducer::SyncProducer(Network *network) : Sync(network), sync_sent(false)
{
    unsigned char data = 0;
    this->frame = Frame(ID_SYNC, &data, 0);
}

void SyncProducer::start(int period)
{
    if (this->running)
    {
        this->stop();
    }
    this->sync_sent = false;
    this->timer.attach(callback(this, &SyncProducer::timer_callback), period/1000.0f);
    this->running = true;
}

void SyncProducer::stop()
{
    this->timer.detach();
    this->running = false;
}

void SyncProducer::send()
{
    if (this->network->is_active())
    {
        if (LED_SYNC)
        {
            this->led = !this->led;
        }
        this->network->send(this->frame);
        this->sync_sent = true;
    }
}

void SyncProducer::timer_callback()
{
    this->send();
}

bool SyncProducer::sent()
{
    if (this->sync_sent == false)
    {
        return false;
    }
    else
    {
        this->sync_sent = false;
        return true;
    }
}

SyncConsumer::SyncConsumer(Network *network) : Sync(network), sync_received(false)
{
    this->network->register_sync_handler(callback(this, &SyncConsumer::received));
}

void SyncConsumer::start()
{
    this->sync_received = false;
    this->running = true;
}

void SyncConsumer::stop()
{
    this->running = false;
}

bool SyncConsumer::is_received()
{
    if (this->sync_received == false)
    {
        return false;
    }
    else
    {
        this->sync_received = false;
        return true;
    }
}

void SyncConsumer::received(Frame frame)
{
    if (this->running)
    {
        if (LED_SYNC)
        {
            this->led = !this->led;
        }
        this->sync_received = true;
    }
}
