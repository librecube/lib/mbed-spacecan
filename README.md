# Mbed SpaceCAN

This is a C++ implementation of the
[SpaceCAN protocol](https://wiki.librecube.org/index.php?title=SpaceCAN)
for embedded systems. It utilizes the [mbed framework](https://os.mbed.com).

## Usage

We recommended to use the [platformio IDE](https://docs.platformio.org).
Clone this repository into ``lib/spacecan`` of your project.

## Contribute

- Issue Tracker: https://gitlab.com/librecube/lib/mbed-spacecan/-/issues
- Source Code: https://gitlab.com/librecube/lib/mbed-spacecan

To learn more on how to successfully contribute please read the contributing
information in the [LibreCube guidelines](https://gitlab.com/librecube/guidelines).

## Support

If you are having issues, please let us know. Reach us at
[Matrix](https://app.element.io/#/room/#librecube.org:matrix.org)
or via [Email](mailto:info@librecube.org).

## License

The project is licensed under the MIT license. See the [LICENSE](./LICENSE.txt) file for details.
